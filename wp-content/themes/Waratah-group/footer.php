<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
?>
  
</div>
<div class="footer">
 	<div class="footer-logo">
		<img src="<?php echo get_field('footer_logo','option'); ?>">
	</div>
	<div class="footer-text">
		<?php echo get_field('footer_text','option');?>
	</div>
	<div class="footer-social">
		
		<ul>
			<li> <a href="<?php echo get_field('linked_in');?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/linked_in.png"> </a></li>

			<li> <a href="<?php echo get_field('facebook');?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/facebook.png"> </a></li>

			<li> <a href="<?php echo get_field('instagram');?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/instagram.png"> </a></li>
		</ul>
	</div>

	 
		 

 
</div>
 

<?php wp_footer(); ?>
<script>
	//new UISearch( document.getElementById( 'sb-search' ) );
</script>
</body>
</html>