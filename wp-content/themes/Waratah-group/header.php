	<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
?>
	<!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title><?php wp_title( '&#124;', true, 'right' ); ?></title>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<?php wp_head(); ?>
		<?php wp_head(); ?>
		 
		
	
	</head>

<body <?php body_class(); ?>>

<?php $backheader =  get_field('header_background','option'); ?>
 
<div class="top-header"> 
	
	<div class="header-logo">
		<div class="top-logo"> <a href="<?php echo home_url( ); ?>" > <img src="<?php echo get_field('top_logo','option');?>"></a></div>
		 
	</div>
<div id="nav-container" >
	<div class="menu">
	<?php wp_nav_menu( array(
					'container'       => 'div',
					'container_class' => 'main-nav',
					'theme_location'  => 'top-menu',
					'menu'  => 'Top Menu'
				)
			);

			?>
	</div>
</div>

			</div>
		
	 </div>
	
</div>

 
<!-- Nav Wrap END -->

