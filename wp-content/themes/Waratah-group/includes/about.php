<div class="container about">

  
    <div class="col-xs-12 ">
     <h1><?php echo get_field('title','option'); ?> </h1>
     <?php echo get_field('about_text','option');?>
<div class="border"> </div>
    
    <div class="services">
    	<?php $services = get_field('services','option'); 

    	foreach($services as $serv) { ?>

    		<div class="col-md-3 col-sm-4 col-xs-6 serv-name" style="border-color: <?php echo $serv['color'];?>;"><?php echo $serv['name']; ?></div>

    <?php	} ?>
    </div>
   
     </div>
    
  
</div>
